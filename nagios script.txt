=======================================================================
Nagios monitors your entire IT infrastructure to ensure systems,applications, services, and business processes are functioning properly.




# Install nagios dependencies
yum install -y epel-release

#install nagios packages
yum -y install nagios nagios-plugins-all nagios-plugins-nrpe nrpe httpd php

# Enable and start nagios and httpd. Ensure no service is running on port 80 for httpd to start
systemctl enable httpd  && systemctl enable nagios

systemctl start httpd  && systemctl start nagios


=================================CREATE SWAP================================================

swapon --show

dd if=/dev/zero of=/swapfile-additional bs=1M count=2048

chmod 600 /swapfile-additional

 mkswap /swapfile-additional

 vi /etc/fstab

 /swapfile-additional swap swap 0 0

 mount -a

 swapon -a

 swapon --show

 free -m

 ================================================================================================


# SET NAGIOS PASSWORD
 htpasswd -c /etc/nagios/passwd nagiosadmin


# TO ACCESS, GO TO WEB INTERFACE WITH MACHINE_IP/nagios
Username: nagiosadmin
password: [SET_ABOVE]








===============================#INSTALL NAGIOS ON CLIENT===============================================

yum -y install nagios nagios-plugins-all nrpe

systemctl start nrpe && systemctl status nrpe

# EDIT THE FILE vi /etc/nagios/nrpe.cfg

allowed_hosts=monitoring_server_ip

# Set firewall rule to allow nrpe communication between server and client
iptables -N NRPE
iptables -I INPUT -s 0/0 -p tcp --dport 5666 -j NRPE
iptables -I NRPE -s 192.168.56.101 -j ACCEPT
iptables -A NRPE -s 0/0 -j DROP
/etc/init.d/iptables save

# Start nrpe service
systemctl start nrpe

===========================================================================================================






==============================GO BACK TO NAGIOS SERVER======================================================================


# OPEN  /etc/nagios/nagios.cfg and uncomment  the following line
 cfg_dir=/etc/nagios/servers

# Make the directory
mkdir /etc/nagios/servers/

# Create the following configuration file to specify kinds of services to monitor
vi /etc/nagios/servers/client.cfg

# set the folder permissions correctly and restart Nagios
chown -R nagios. /etc/nagios/

systemctl restart nagios


# TO ACCESS, GO TO WEB INTERFACE WITH MACHINE_IP/nagios
Username: nagiosadmin
password: [SET_ABOVE]










